package ma.octo.connect4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;


import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AnalyseurTest {

    @Mock
    private Grille grille;
    @InjectMocks
    private Analyseur analyseur;


    @Test
    public void ShouldReturnRedWhen4RedsAreAlignedHorizontallyInFirstLineOfCols1234() {



        List<List<String>> lines = List.of(List.of(".", ".", ".", ".", ".", ".", "."),
                List.of(".", ".", ".", ".", ".", ".", ".")
                , List.of(".", ".", ".", ".", ".", ".", "."), List.of(".", ".", ".", ".", ".", ".", "."),
                List.of("B", "B", "B", ".", ".", ".", "."), List.of("R", "R", "R", "R", ".", ".", "."));

        when(grille.getLines()).thenReturn(lines);

        String result = analyseur.verifyHorizontally(grille).get();

        Assert.assertEquals("R", result);
    }
    @Test
    public void shouldReturnBlueWhen4BluesAreAlignedHorizontallyInLine2OfCols4567() {



        List<List<String>> lines = List.of(List.of(".", ".", ".", ".", ".", ".", "."),
                List.of(".", ".", ".", ".", ".", ".", ".")
                , List.of(".", ".", ".", ".", ".", ".", "."), List.of(".", ".", ".", ".", ".", ".", "."),
                List.of(".", ".", ".", "B", "B", "B", "B"), List.of("R", "R", "R", "R", ".", ".", "."));

        when(grille.getLines()).thenReturn(lines);

        String result = analyseur.verifyHorizontally(grille).get();

        Assert.assertEquals("B", result);
    }

    @Test
    public void ShouldReturnRedWhen4RedsAreAlignedVerticallyInCol1FromFirstLine() {

        List<List<String>> columns = List.of(List.of(".",".","R","R","R","R"),
                List.of(".", ".", ".", ".", ".", ".", ".")
                , List.of("R", ".", ".", ".", ".", ".", "."), List.of("R", ".", ".", ".", ".", ".", "."),
                List.of("R", ".", ".", ".", ".", ".", "B"), List.of("R", ".", ".", "R", ".", ".", "."));

        when(grille.getColumns()).thenReturn(columns);

        String result = analyseur.verifyVertically(grille).get();


        Assert.assertEquals("R", result);
    }

    @Test
    public void ShouldReturnBlueWhen4BluesAreAlignedVerticallyInCol3FromSecondLine() {



        List<List<String>> columns = List.of(List.of(".",".",".",".",".","R"),
                List.of(".", ".", ".", ".", ".", ".", ".")
                , List.of("R", ".", "B", "B", "B", "B", "."), List.of("R", ".", ".", ".", ".", ".", "."),
                List.of("R", ".", ".", ".", ".", ".", "B"), List.of("R", ".", ".", "R", ".", ".", "."));

        when(grille.getColumns()).thenReturn(columns);

        String result = analyseur.verifyVertically(grille).get();


        Assert.assertEquals("B", result);
    }



    @Test
    public void ShouldReturnRedWhen4RedsAreAlignedDiagonallyUpBottomFromCol3Lin4() {

//        List<List<String>> columns = List.of(List.of("."),
//                List.of(".", ".")
//                ,List.of("R", ".", "."), List.of("R", ".", ".", "."),
//                List.of("R", ".", ".", ".", "."), List.of("R", ".", ".", "R", ".", ".")
//                ,List.of("R", ".", ".", "R", ".", ".")
//                ,List.of("R", ".", ".", ".", "."),List.of("R", ".", ".", ".")
//                , List.of("R", ".", "."),  List.of(".", "."),List.of(List.of("."));
//
//        when(grille.getColumns()).thenReturn(columns);
//
//        String result = analyseur.verifyVertically(grille).get();
//
//
//        Assert.assertEquals("R", result);
    }

}
