package ma.octo.connect4;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationTest {

    @Mock
    private Grille grille;

    @Mock
    private Analyseur analyseur;

    @Mock
    private View view;

    @InjectMocks
    private Application application;

    @Test
    public void shouldCallDisplayMethodFromGrille() throws Exception {

        application.play(grille,view,analyseur);
        verify(view).displayGrid(grille);
    }
    @Test
    public void shouldCallDisplayInsertMessage() throws Exception {

        application.play(grille,view,analyseur);
        verify(view).displayMessage("JAUNE COLOMNE [1-7]");
    }
    @Test
    public void shouldReadInputColumn() throws Exception {

        application.play(grille,view,analyseur);
        verify(view).readNextColumn("7");
    }
    @Test
    public void shouldInsertTokenIntoGrid() throws Exception {

        application.play(grille,view,analyseur);
        verify(grille).insertInColumn(5,"R");
    }
    @Test
    public void shouldInvokeAnalyseurMethod() throws Exception {


            application.play(grille,view,analyseur);
            verify(analyseur).verifyVertically(grille);
            verify(analyseur).verifyHorizontally(grille);
            verify(analyseur).verifyDiagonally(grille);

    }




}
