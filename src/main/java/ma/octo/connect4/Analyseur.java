package ma.octo.connect4;

import java.util.List;
import java.util.Optional;

public class Analyseur {

    public Optional<String> verifyHorizontally(Grille grille) {

        List<List<String>> lines=grille.getLines();
        for (List<String> line:lines) {
            for (int i=0 ; i <= 3;i++) {
                if (checkConnectivity(line, i)) return Optional.of(line.get(i));

            }
        }
        return Optional.empty();
    }

    public Optional<String> verifyVertically(Grille grille) {
        List<List<String>> columns=grille.getColumns();
        for (List<String> column:columns) {
            for (int i=0 ; i <= 2;i++) {
                if (checkConnectivity(column, i)) return Optional.of(column.get(i));

            }

        }
        return Optional.empty();

    }

    public Optional<String> verifyDiagonally(Grille grille) {

        return Optional.empty();

    }

    private boolean checkConnectivity(List<String> column, int i) {
        if(column.get(i)!="." && column.get(i) == column.get(i+1) &&
                column.get(i+1)==column.get(i+2) && column.get(i+2)==column.get(i+3)){
            return true;
        }
        return false;
    }
}
