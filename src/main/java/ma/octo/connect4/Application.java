package ma.octo.connect4;

import java.util.Optional;

public class Application {


    public void play(Grille grille,View view,Analyseur analyseur) throws Exception {

        boolean enCours = true;
        while(enCours){

            view.displayGrid(grille);
            view.displayMessage("JAUNE COLOMNE [1-7]");
            view.readNextColumn("7");
            grille.insertInColumn(5,"R");
            view.displayMessage("ROUGE COLOMNE [1-7]");
            Optional<String> result=analyseur.verifyVertically(grille);
            Optional<String> result1=analyseur.verifyHorizontally(grille);
            Optional<String> result2=analyseur.verifyDiagonally(grille);
            if(result.isPresent() ){
                enCours=false;
                view.displayMessage("le joueur"+result);
            }
            if(result1.isPresent() ){
                enCours=false;
                view.displayMessage("le joueur"+result1);
            }
            if(result2.isPresent() ){
                enCours=false;
                view.displayMessage("le joueur"+result2);
            }
            if(grille.isFull()){
                enCours=false;
                view.displayMessage("Match null");
            }
            enCours=false;

        }

    }


}
